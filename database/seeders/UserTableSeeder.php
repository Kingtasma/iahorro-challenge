<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $userFaker = \Faker\Factory::create();

        for ($i = 0; $i < 5; $i++) {
            User::create([
                'firstname' => $userFaker->firstName,
                'lastname' => $userFaker->lastName,
                'email' => $userFaker->email,
                'password' => $userFaker->password
            ]);
        }
    }
}
