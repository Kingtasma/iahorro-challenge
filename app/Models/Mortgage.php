<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mortgage extends Model
{
    use HasFactory;

    protected $fillable = [
        'purchase_price',
        'savings',
        'user_id',
        'client_id'
    ];

    protected $hidden = [
        'user_id',
        'client_id',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function percentage()
    {
        return $this->percentage = $this->savings / $this->purchase_price * 100;
    }
}
