<?php

namespace App\Http\Controllers;

use App\Models\User;

class UserController extends Controller
{
    public function getRandomUser(): User
    {
        return User::inRandomOrder()->first();
    }

    public function getUserMortgagesByDate(User $user, String $dateFrom, String $dateTo)
    {
        $returnedData = [];
        $returnedCode = 200;

        $mortgages = $user->mortgages->whereBetween('created_at', [$dateFrom, $dateTo]);
        foreach ($mortgages as $mortgage) {
            $mortgage->client;
            $mortgage->percentage();
        }

        if ($mortgages) {
            $returnedData = [
                "success" => true,
                "parameters" => [
                    "mortgages" => $mortgages
                ]
            ];
        } else {
            $returnedData = [
                "success" => false,
                "parameters" => []
            ];
            $returnedCode = 400;
        }
        return response()->json($returnedData, $returnedCode);
    }
}
