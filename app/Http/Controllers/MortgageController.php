<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mortgage;
use App\Models\Client;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class MortgageController extends Controller
{
    public function store(Request $request)
    {
        $responseData = [];
        $responseCode = 200;
        $validate = $this->createMortgageValidation($request);
        if ($validate->fails()) {
            $responseData = [
                'success' => false,
                'errors' => $validate->errors(),
            ];
            $responseCode = 400;
        } else {
            $client = Client::updateOrCreate([
                "firstname" => $request->client['firstname'],
                "lastname" => $request->client['lastname'],
                "email" => $request->client['email'],
                "phone" => $request->client['phone'],
            ]);
            $user = User::inRandomOrder()->first();

            $mortgage = Mortgage::create([
                'purchase_price' => $request->mortgage['purchase_price'],
                'savings' => $request->mortgage['savings'],
                'client_id' => $client->id,
                'user_id' => $user->id,
            ]);

            $responseData = [
                'sucsess' => true,
                'parameters' => [
                    'client' => $client,
                    'mortgage' => $mortgage,
                    'user' => $user
                ]
            ];
            $responseCode = 200;
        }

        return response()->json($responseData, $responseCode);
    }

    private function createMortgageValidation(Request $request)
    {
        return Validator::make(
            $request->all(),
            [
                'client' => 'bail|required',
                'client.firstname' => 'required|alpha',
                'client.lastname' => 'required|alpha',
                'client.email' => 'required|email',
                'client.phone' => 'required|regex:/^[0-9]{9}$/',
                'mortgage' => 'required',
                'mortgage.purchase_price' => 'required|numeric|min:0',
                'mortgage.savings' => 'required|numeric|min:0',
            ],
            [
                'required' => 'The :attribute is required',
                'numeric' => 'The :attribute must be a numeric value',
                'min' => 'The :attribute min value is :value'
            ]
        );
    }
}
